<?php declare(strict_types=1);

namespace Plugin\jtl_google_recaptcha;

use JTL\Helpers\Request;
use JTL\Plugin\PluginInterface;

/**
 * Class AbstractCaptcha
 * @package Plugin\jtl_google_recaptcha
 */
abstract class AbstractCaptcha implements CaptchaInterface
{
    private const VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';

    /** @var PluginInterface */
    private $plugin;

    /** @var String */
    private $siteKey;

    /** @var String */
    private $secretKey;

    /**
     * AbstractCaptcha constructor.
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin)
    {
        $this->plugin    = $plugin;
        $this->secretKey = $this->plugin->getConfig()->getValue('jtl_google_recaptcha_secretkey');
        $this->siteKey   = $this->plugin->getConfig()->getValue('jtl_google_recaptcha_sitekey');
    }

    /**
     * @inheritDoc
     */
    public function getSiteKey(): ?string
    {
        return $this->siteKey;
    }

    /**
     * @inheritDoc
     */
    public function getSecretKey(): ?string
    {
        return $this->secretKey;
    }

    /**
     * @return PluginInterface
     */
    protected function getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @inheritDoc
     */
    public function isConfigured(): bool
    {
        return !empty($this->secretKey) && !empty($this->siteKey);
    }

    /**
     * @inheritDoc
     */
    public function validate(array $requestData): bool
    {
        if (!$this->isConfigured()) {
            return true;
        }

        if (empty($requestData['g-recaptcha-response'])) {
            return false;
        }

        $jResponse = Request::make_http_request(self::VERIFY_URL, 5, [
            'secret'   => $this->getSecretKey(),
            'response' => $requestData['g-recaptcha-response'],
        ]);
        if (\is_string($jResponse)) {
            $result = \json_decode($jResponse, false);
            if (\json_last_error() === \JSON_ERROR_NONE) {
                return isset($result->success) && $result->success;
            }
        }

        return false;
    }
}
