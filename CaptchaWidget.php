<?php declare(strict_types=1);
namespace Plugin\jtl_google_recaptcha;

use Exception;
use JTL\Plugin\PluginInterface;
use JTL\Shop;

/**
 * Class CaptchaWidget
 * @package Plugin\jtl_google_recaptcha
 */
class CaptchaWidget extends AbstractCaptcha
{
    /** @var string */
    private $theme;

    /** @var string */
    private $size;

    /**
     * CaptchaWidget constructor.
     * @param PluginInterface $plugin
     */
    public function __construct(PluginInterface $plugin)
    {
        parent::__construct($plugin);

        $config      = $this->getPlugin()->getConfig();
        $this->theme = $config->getValue('jtl_google_recaptcha_theme');
        $this->size  = $config->getValue('jtl_google_recaptcha_size');
    }

    /**
     * @return string
     */
    public function getTheme(): string
    {
        return $this->theme;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @inheritDoc
     */
    public function getMarkup(): string
    {
        $plugin = $this->getPlugin();
        try {
            return Shop::Smarty()
                ->assign('reCaptchaSitekey', $this->getSiteKey())
                ->assign('reCaptchaPopupTitle', $plugin->getLocalization()->getTranslation(
                    'recaptcha_popup_title'
                ))
                ->assign('reCaptchaTheme', $this->getTheme())
                ->assign('reCaptchaSize', $this->getSize())
                ->assign('reCaptchaJSPath', $plugin->getPaths()->getFrontendURL() . 'js/recaptcha.js')
                ->fetch($plugin->getPaths()->getFrontendPath(). '/template/recaptcha_widget.tpl');
        } catch (Exception $e) {
            return \__('Cannot render captcha');
        }
    }
}
