#!/usr/bin/env bash

PROJECT_NAME=$1;
TAG=$2;
VERSION="${TAG//[\/\.]/-}";
FILENAME="jtl_google_recaptcha-${VERSION}.zip";
ARCHIVE_PATH="${3}/${FILENAME}";
ZIP_PATH=$4;
VCS_REG_TAGS="v?([0-9]{1,})\\.([0-9]{1,})\\.([0-9]{1,})(-(alpha|beta|rc)(\\.([0-9]{1,}))?)?";

echo "";
echo "Create zip of build '${TAG}'...";

zip -r -q ${ARCHIVE_PATH} ${ZIP_PATH} -x \*.git* \*.idea* \*docs/* \*patch-dir-* \*tests/* \*build/* \*node_modules/* \*.asset_cs \*.php_cs \*.travis.yml \*phpunit.xml;
echo "  ${FILENAME}";
echo "";

#if [[ ${TAG} =~ ${VCS_REG_TAGS} ]]; then
#    cp -r ${ARCHIVE_PATH} ${3}/latest/jtl_google_recaptcha.zip
#fi