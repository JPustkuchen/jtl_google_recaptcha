<?php declare(strict_types=1);
namespace Plugin\jtl_google_recaptcha\backend;

use Exception;
use JTL\Alert\Alert;
use JTL\Helpers\Text;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_google_recaptcha\Captcha;

/**
 * Class CustomLink
 * @package Plugin\jtl_google_recaptcha\backend
 */
class CustomLink
{
    /** @var PluginInterface */
    private $plugin;

    /** @var object */
    private $menu;

    /** @var string */
    private $task;

    /** @var array */
    private $requestData;

    /** @var string */
    private $output = '';

    /** @var JTLSmarty */
    private $smarty;

    /**
     * CustomLink constructor.
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     * @param JTLSmarty       $smarty
     */
    private function __construct(PluginInterface $plugin, object $menu, array $requestData, JTLSmarty $smarty)
    {
        $this->plugin      = $plugin;
        $this->menu        = $menu;
        $this->requestData = Text::filterXSS($requestData);
        $this->task        = \strtolower($menu->name);
        $this->smarty      = $smarty;
    }

    /**
     * @param PluginInterface $plugin
     * @param object          $menu
     * @param array           $requestData
     * @param JTLSmarty       $smarty
     * @return CustomLink
     */
    public static function handleRequest(
        PluginInterface $plugin,
        object $menu,
        array $requestData,
        JTLSmarty $smarty
    ): self {
        $instance = new static($plugin, $menu, $requestData, $smarty);
        if ($instance->controller()) {
            return $instance->render();
        }

        return $instance;
    }

    /**
     * @return void
     */
    private function taskTesting(): void
    {
        $captcha = Captcha::getCaptcha(
            $this->getPlugin()->getConfig()->getValue('jtl_google_recaptcha_type'),
            $this->getPlugin()
        );
        $data    = $this->getRequestData();
        try {
            if (isset($data['test_var'])) {
                $this->smarty->assign('reCaptchaValid', $captcha->validate(Text::filterXSS($_POST)))
                    ->assign('reCaptchaTest', Text::htmlentities($data['test_var']));
            }
            $pluginURL = \method_exists($this->getPlugin()->getPaths(), 'getBackendURL')
                ? $this->getPlugin()->getPaths()->getBackendURL()
                : Shop::getAdminURL() . '/plugin.php?kPlugin=' . $this->getPlugin()->getID();
            $this->smarty->assign('pluginURL', $pluginURL)
                ->assign('reCaptchaConfig', $captcha->isConfigured())
                ->assign('reCaptcha', $captcha->getMarkup());
        } catch (Exception $e) {
            Shop::Container()->getAlertService()->addAlert(Alert::TYPE_ERROR, $e->getMessage(), 'smartyError');
        }
    }

    /**
     * @return bool
     */
    protected function controller(): bool
    {
        if ($this->task === 'testing') {
            $this->taskTesting();
        }

        return true;
    }

    /**
     * @return string
     */
    protected function getTemplate(): string
    {
        return 'template/' . $this->task . '.tpl';
    }

    /**
     * @return PluginInterface
     */
    public function getPlugin(): PluginInterface
    {
        return $this->plugin;
    }

    /**
     * @return object
     */
    public function getMenu(): object
    {
        return $this->menu;
    }

    /**
     * @return array
     */
    public function getRequestData(): array
    {
        return $this->requestData;
    }

    /**
     * @return string
     */
    public function getOutput(): string
    {
        return $this->output;
    }

    /**
     * @return static
     */
    public function render(): self
    {
        try {
            $this->output = $this->smarty->assign('requestData', $this->getRequestData())
                ->assign('kPlugin', $this->getPlugin()->getID())
                ->assign('kPluginAdminMenu', $this->getMenu()->kPluginAdminMenu)
                ->fetch($this->getPlugin()->getPaths()->getAdminPath() . $this->getTemplate());
        } catch (Exception $e) {
            $this->output = $e->getMessage();
        }

        return $this;
    }
}
