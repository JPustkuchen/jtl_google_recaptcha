<?php declare(strict_types=1);
namespace Plugin\jtl_google_recaptcha;

use Exception;
use JTL\Shop;

/**
 * Class CaptchaInvisible
 * @package Plugin\jtl_google_recaptcha
 */
class CaptchaInvisible extends AbstractCaptcha
{
    /**
     * @inheritDoc
     */
    public function getMarkup(): string
    {
        $plugin = $this->getPlugin();
        try {
            return Shop::Smarty()
                ->assign('reCaptchaSitekey', $this->getSiteKey())
                ->assign('reCaptchaJSPath', $plugin->getPaths()->getFrontendURL() . 'js/recaptcha.js')
                ->fetch($plugin->getPaths()->getFrontendPath(). '/template/recaptcha_invisible.tpl');
        } catch (Exception $e) {
            return \__('Cannot render captcha');
        }
    }
}
