<?php declare(strict_types=1);
namespace Plugin\jtl_google_recaptcha;

use JTL\Plugin\PluginInterface;

/**
 * Class Captcha
 * @package Plugin\jtl_google_recaptcha
 */
class Captcha
{
    /**
     * @param string          $type
     * @param PluginInterface $plugin
     * @return CaptchaInterface
     */
    public static function getCaptcha(string $type, PluginInterface $plugin): CaptchaInterface
    {
        switch ($type) {
            case 'invisible':
                return new CaptchaInvisible($plugin);
            case 'widget':
            default:
                return new CaptchaWidget($plugin);
        }
    }
}
